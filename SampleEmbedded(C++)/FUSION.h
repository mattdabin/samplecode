/**
 * Umoform Sensor Fusion Library
 *  
 * * Copyright (C) 2018 Matthew Dabin
 *
 * 
 **/
#ifndef FUSION_h
#define FUSION_h

#include "arduino.h"

extern "C" {
  #include <math.h>
};

class FUSION {
public:
    
    // Constructor
    FUSION(float beta, float magnetometer_norm_lower, float magnetometer_norm_upper){
        global_beta = beta;
        mag_norm_ll = magnetometer_norm_lower;
        mag_norm_lu = magnetometer_norm_upper;
    }
    void madgwick_9DOF(float g[3], float a[3], float m[3]);
    // void madgwick_6DOF(float g[3], float a[3]);
    void set_beta(float beta);

    float accelerometer_norm;   // Magnitude Scale of Accelerometer
    float magnetometer_norm;    // Magnitude Scale of Magnetometer
    float q[4] = {0,0,0,1};     // Quaternions

private:

    unsigned long first_time = 0;
    unsigned long second_time, diff_time;
    float mag_norm_ll;          // Magnetometer magnitude scale upper limit
    float mag_norm_lu;          // Magnetometer magnitude scale lower limit
    float global_beta;          // Sensitivity of Magnetometer Correction
};

#endif

