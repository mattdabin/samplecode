clear
clc

% Test Script for cross spectral matrix noise removal using matrix diagonal
% deletion.
% Matthew Dabin 2018 RMIT

% Inputs Parameters
Fs = 2000;                          % Sampling frequency
M = 2;                             % Number of Sensor Channels

T = 1/Fs;                           % Sampling period
L = Fs*30;                          % Number of samples
t = (0:L-1)*T;                      % Time vector
LN = 1000;                          % Frame Length
nFFT = 2^nextpow2(LN);              % Number fft points
f = Fs*(0:(nFFT/2))/nFFT;           % Frequency Vector
win = Hann(LN);                     % Window
inc = LN/2;                         % Overlap

% Simulate coherent signal with added White Guassian Noise
for index = 1:M
    x(index,:) = 0.1*sin(2*pi*100*t)+sqrt(1)*randn(size(t));
%     x(index,:) = awgn(x(index,:),5);
end

% Compute cross spectral matrix
[C] = CSM(x, nFFT, win, inc);

% Diagonal Removal
DRmask = repmat(1-eye(M),[1 1 nFFT]);
Cdr = C.*DRmask;

% Array Steering Vectors
Gt = repmat(ones(M,1),[1 1 nFFT]);
G = repmat(ones(1,M),[1 1 nFFT]);

% Compute CSM output
for freq = 1:nFFT
    S(freq) = G(:,:,freq)*C(:,:,freq)*Gt(:,:,freq);
    Sdr(freq) = G(:,:,freq)*Cdr(:,:,freq)*Gt(:,:,freq);
end

% Normalise by num microphone channels
S = S./(M^2);
Sdr = Sdr./(M^2-M); % Compensate for DR removed

figure(1)
title('Single-Sided Amplitude Spectrum of X(t)')
% plot(f,abs(PA)) 
plot(f,2*abs(S(1:nFFT/2+1)).^0.5, f,2*abs(Sdr(1:nFFT/2+1)).^0.5) 
xlabel('f (Hz)')
ylabel('|P1(f)|')
legend('S', 'SDR');
grid on

function [C] = CSM(x, nFFT, win, inc)
    % Use statistical averaging to reduce the noise variance 
    [M, ~] = size(x);

    % Prep n channels into K frames
    for i = 1:M
        xf = Enframe(x(i,:),win,inc);
        X(i,:,:) = fft(xf',nFFT)./(sum(win));
    end

    % M - Channels, nFFT - number of FFT points, K - Frames
    [M, nFFT, K]  = size(X);

    % Compute narrow band cross spectral matrix
    A = double(zeros(M,M,nFFT,K));
    for freq = 1:nFFT
        for frame = 1:K
                   Xfi = double(squeeze(X(:,freq,frame)));
                   A(:,:,freq,frame)  = Xfi*(Xfi)';
        end
    end

    % Expectation and normalise by number of frames
    C = sum(A,4)./(K);
end

function [win] = Hann(Length)

    if ~isequal(fix(Length), Length) 
        error('Length must be integer.')
    end

    win = 0.5 - 0.5*cos(2*pi * (1 : Length)' / (Length-1));
    
end

function f=Enframe(x,win,inc)
    %ENFRAME split signal up into (overlapping) frames: one per row. F=(X,WIN,INC)

    nx=length(x);
    nwin=length(win);
    if (nwin == 1)
       len = win;
    else
       len = nwin;
    end
    if (nargin < 3)
       inc = len;
    end
    nf = fix((nx-len+inc)/inc);
    f=zeros(nf,len);
    indf= inc*(0:(nf-1)).';
    inds = (1:len);
    f(:) = x(indf(:,ones(1,len))+inds(ones(nf,1),:));
    if (nwin > 1)
        w = win(:)';
        f = f .* w(ones(nf,1),:);
    end
end
