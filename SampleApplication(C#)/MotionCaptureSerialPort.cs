#region Copyright & License
/* Copyright (C) Umoform, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Matthew Dabin <mattdabin@gmail.com>, September 2018
 * 
 * Version 0.5.1
 */
#endregion

 
 /*****************
  * 
  * MotionCaptureSerialPort Class:
  * - Connects to embedded bluetooth device.
  * - Recieved byte stream and loads into circular buffer.
  * - Decodes into motion data structure.
  * - Stores into public MotionQueue.
  * 
  * Dependancies:
  * - ByteQueue (Custom Circular Byte Buffer)
  * - 
  * Todo:
  * - Contructor with no port name search for correct device
  * - Search for serial port names
  * - Known device name found
  * - Error log and remove requirement for UnityEngine
  * - 
  *****************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;

namespace Assets
{
    public class MotionCaptureSerialPort
    {
        private SerialPort port;
        private string portName = "UMOX";
        private readonly int baudRate = 115200;
        private const int readTimeout = 10;
        private const int writeTimeout = 10;
        private const int threadPoolSleepTime = 10;
        private Thread thread;
        private bool stopRequested = false;
        public bool strData = false;
        public Queue inputQueueMotion;
        private ByteQueue byteQueue;

        public MotionCaptureSerialPort(string portName) {
            this.portName = portName;
            byteQueue = new ByteQueue();
        }

        public MotionCaptureSerialPort()
        {
            byteQueue = new ByteQueue();
        }

        ~MotionCaptureSerialPort() {
            stopRequested = true;
            port.Close();
            port.Dispose();
        }

        public void Initialise() {
            inputQueueMotion = Queue.Synchronized(new Queue());
            thread = new Thread(ThreadLoop);
            thread.Start();
        }

        // .net serial port unreliable interrupt ready rely on polling
        private void ThreadLoop() {
            while (true && !stopRequested) {
                port = new SerialPort(portName, baudRate) {
                    ReadTimeout = readTimeout,
                    WriteTimeout = writeTimeout
                };
                OpenPort();
                if (!port.IsOpen) { continue; } else {
                    port.DiscardInBuffer();
                }
                while (port.IsOpen && !stopRequested) {
                    if (strData) {
                        ReadLine();
                    } else {
                        ReadASync();
                    }
                    Thread.Sleep(threadPoolSleepTime);
                }
            }
        }

        public int FramesInQueue() {
            return inputQueueMotion.Count;
        }

        private void OpenPort()
        {
            try
            {
                port.Open();
                Debug.Log("Port Opened");
            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message);
            }
        }

        public void SetPose() {
            try
            {
                port.WriteLine("o");
            }
            catch (Exception) { }
        }

        public void RequestStop()
        {
            try
            {
                stopRequested = true;
                port.Close();
            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message);
            }
        }

        private void ReadLine()
        {
            try
            {
                string str = port.ReadLine();
                Debug.Log(str);
            }
            catch (Exception) { }
        }

        private void ReadASync()
        {
            byte[] buffer = new byte[4000];
            Action kickoffRead = null;
            kickoffRead = delegate {
                port.BaseStream.BeginRead(buffer, 0, buffer.Length, delegate (IAsyncResult ar) {
                    try
                    {
                        int actualLength = port.BaseStream.EndRead(ar);
                        byte[] received = new byte[actualLength];
                        Buffer.BlockCopy(buffer, 0, received, 0, actualLength);
                        RaiseAppSerialDataEvent(received);
                    }
                    catch (IOException exc)
                    {
                        HandleAppSerialError(exc);
                    }
                    kickoffRead();
                }, null);
            };
            kickoffRead();
        }

        private void RaiseAppSerialDataEvent(byte[] receivedBytes)
        {
            byteQueue.Enqueue(receivedBytes, 0, receivedBytes.Length);

            // Check ByteBuffer for available packets.
            while (byteQueue.Length >= 22)
            {
                if ((0xFF == byteQueue.PeekOne(20)) && (0x0F == byteQueue.PeekOne(21)))
                {
                    byte[] peakBytes = new byte[20];
                    byteQueue.Peak(peakBytes, 0, 20);
                    byteQueue.Clear(22);
                    DataPacket dataPacket = ByteArrayToStructure<DataPacket>(peakBytes);
                    inputQueueMotion.Enqueue(dataPacket);

                    if (dataPacket.sensorIndex == 0) {
                        Debug.Log("Primary Recieved");
                    }
                }
                else {
                    byteQueue.Clear(1);
                    Debug.Log("Packet Loss");
                }
            }
        }

        private void DecodeByte(byte[] received) {
           
            var table = (Encoding.Default.GetString(received, 0, received.Length - 1)).Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in table)
                Debug.Log(s);
        }

        struct DataPacket
        {
            public byte sensorIndex;

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public float[] quaternion;
        }

        private static T ByteArrayToStructure<T>(byte[] bytes) where T : struct
        {
            var handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            try
            {
                return (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(T));
            }
            finally
            {
                handle.Free();
            }
        }

        private void HandleAppSerialError(IOException exc)
        {
            throw new NotImplementedException();
        }
    }
}

/*************************************
 * Reference Material
 * 
 * http://www.sparxeng.com/blog/software/must-use-net-system-io-ports-serialport
 * https://stackoverflow.com/questions/11654562/how-convert-byte-array-to-string
 * 
 **************************************/